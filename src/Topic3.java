import java.util.Scanner;

public class Topic3 {
    String globalVar = "ini global variable";
    public static String classVariable = "class variable";
    public String instanceVariable = "instance variable";

    public static void main(String[] args) {
        System.out.println("===== tipe data char =====");
        char mencoba = '\77';
        char tandaTanya = '?';
        System.out.println(mencoba);
        System.out.println(tandaTanya);

        System.out.println("Kalkulator");
        Scanner input = new Scanner(System.in);
        System.out.println();

        System.out.println("==== Operator ====");
        System.out.println("==== Compund Assignment ====");
        int opr1 = 9;
        opr1 += 5; // 9 + 5 --------- opr1 = 14
        System.out.println(opr1);
        opr1 -= 2; // 14 - 2 --------- opr1 = 12
        System.out.println(opr1);
        opr1 *= 2; // 12 * 2 -------- opr1 = 24
        System.out.println(opr1);
        opr1 /= 3; // 24 / 3 -------- opr1 = 8
        System.out.println(opr1);
        opr1 %= 7; // 8 * 7 -------- opr1 = 1
        System.out.println(opr1);

        System.out.println("==== Comparison ====");
        int comp1 = 5;
        int comp2 = 10;
        System.out.println(comp1 == comp2);
        System.out.println(comp1 != comp2);
        System.out.println(comp1 < comp2);
        System.out.println(comp1 <= comp2);

        System.out.println("==== Logical ====");
        int log1 = 1;
        int log2 = 2;
        System.out.println(comp1 == comp2 && log1 != log2); // false

        System.out.println("=== Conditional ===");
        int valA = 5;
        int valB = 6;
        if(valA < valB) {
            System.out.println("valA kurang dari valB nih");
        } else {
            System.out.println("valA ternyata ga kurang dari dari valB");
        }
        System.out.println("check valA < valB done");

        // Conditional if else ternary operator
        System.out.println(valA < valB ? "valA kurang dari valB nih" : "valA ternyata ga kurang dari dari valB");

        valA = 10;
        if (valA > valB) System.out.println("val A lebih dari val B");

        String nilai = "D";
        switch (nilai) {
            case "A" :
                System.out.println("Selamat nilai A");
                break;
            case "B" :
                System.out.println("Kamu dapet nilai B");
                break;
            default:
                System.out.println("ERROR");
                break;
        }

        System.out.println("=== Loop ===");
        int singleData = 100;
        int[] data = {10, 90, 77, 33, 12, 70};
        for(int index = 0, index2 = data.length-1; index < data.length; index++, index2--) {
            System.out.println(data[index]);
        }

        while (singleData % 2 == 0) {
            // Do something
            System.out.println("single data: " + singleData);
            singleData--;
            return;
        }
    }

    public void localVar() {
        String localVariable = "ini adalah local variable";
        System.out.println(localVariable);
        System.out.println(globalVar);
    }

    public void caller() {
        System.out.println(globalVar);
    }

    public static int addition(int a, int b) {
        return a + b;
    }
}
